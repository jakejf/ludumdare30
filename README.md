# Ludum Dare 30 #

### Connected Worlds ###

4 players control ships racing through space. The track is outlined by planets and their gravity wells. If a player falls too far behind, their ship is destroyed. Players can pick up weapons to fire at each other. The last player alive is the winner.

### Created by ###

* Jake Francis
* Josh Armitage
* Rhys Fenton

### Credit to ###

* [Justin Nichol](http://justinnichol.wordpress.com/)
* [Kenney](http://opengameart.org/users/kenney)