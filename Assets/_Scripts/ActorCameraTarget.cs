﻿using UnityEngine;
using System.Collections;

//  Camera controls will go here.
public class ActorCameraTarget : MonoBehaviour
{ 
    void Start()
    {

    }

    void Update()
    {
		GameObject[] players;

		players = GameObject.FindGameObjectsWithTag("Player"); //Find player positions
		float playerMaxX = 0; //Initiate variables
		float playerMinX = 0;
		float playerMaxY = 0;
		float playerMinY = 0;
		foreach(GameObject player in players) { 
			Vector2 pos = player.transform.position; //Find max and min player positions
			if(pos.x>playerMaxX)
				playerMaxX=pos.x;
			if (pos.x<playerMinX)
				playerMinX=pos.x;
			if(pos.y>playerMaxY)
				playerMaxY=pos.y;
			if(pos.y<playerMinY)
				playerMinY=pos.y;
		}
		Vector2 maxPos; //Populate two arrays for averaging later
		Vector2 minPos;
		maxPos.x = playerMaxX;
		maxPos.y = playerMaxY;
		minPos.x = playerMinX;
		minPos.y = playerMinY;

		Vector2 avePos = Vector2.Lerp (maxPos, minPos, 0.5f); //Average the position between max and min
		transform.position = new Vector3(avePos.x,avePos.y,-20); //Make camera position to be 3D

		float aspectRatio = Camera.main.aspect;
		float xDiff = Mathf.Abs (maxPos.x-minPos.x);
		float yDiff = Mathf.Abs (maxPos.y-minPos.y);
		if(xDiff<aspectRatio*yDiff)		//Calculate size of camera & output}
			Camera.main.orthographicSize = yDiff/2+2;
		else
			Camera.main.orthographicSize = xDiff/aspectRatio/2+2;
		}
}
