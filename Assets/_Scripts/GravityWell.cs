﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CircleCollider2D))]
[RequireComponent(typeof(ParticleSystem))]

public class GravityWell : MonoBehaviour
{
    [SerializeField]
    private float gravityMagnitude;

    [SerializeField]
    private float gravityRadius;

    void Awake()
    {
        gameObject.GetComponent<CircleCollider2D>().radius = 1; //needs to be 1 for transform scaling
        if (gravityMagnitude <= 0f)
            Debug.LogError("GravityWell needs a positive magnitude");
        if (gravityRadius <= 0f)
            Debug.LogError("GravityWell needs a positive radius");
        gameObject.transform.localScale = gravityRadius * Vector3.one;
        //
        
    }

    void OnTriggerStay2D(Collider2D c)
    {
        if (c.gameObject.tag == "Player")
        {
            Vector2 cPos = new Vector2(c.transform.position.x, c.transform.position.y);
            Vector2 thisPos = new Vector2(transform.position.x, transform.position.y);
            Vector2 gravVector = thisPos - cPos;
            c.rigidbody2D.AddForce(gravVector.normalized * gravityMagnitude, ForceMode2D.Force);
        }
    }
}
