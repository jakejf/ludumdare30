﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CircleCollider2D))]

public class KillOnCollision : MonoBehaviour
{
    void OnCollisionEnter2D(Collision2D c)
    {
        if (c.gameObject.tag == "Player")
        {
            GameObject.Destroy(c.gameObject);
        }
    }
}
