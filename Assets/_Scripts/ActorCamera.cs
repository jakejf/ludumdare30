﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Camera))]

public class ActorCamera : MonoBehaviour 
{
    [SerializeField]
    private GameObject cameraTarget;

    void Awake()
    {
        if (cameraTarget == null)
            Debug.LogError("CameraTarget is missing from Camera object.");      
    }

	void Update() 
    {
        transform.position = new Vector3(
            cameraTarget.transform.position.x,
            cameraTarget.transform.position.y,
            transform.position.y
            );
	}
}
