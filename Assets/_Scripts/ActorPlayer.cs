﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Sprite))]

/// <summary>
/// Player controller and behavior
/// </summary>
public class ActorPlayer: MonoBehaviour
{
	/// <summary>
	/// 1 - The speed of the ship
	/// </summary>
	public Vector2 speed = new Vector2(50, 50);
	
	// 2 - Store the movement
	private Vector2 movement;

    public int currentWaypoint;
	
	void Update()
	{
		// 3 - Retrieve axis information
		float inputX = Input.GetAxis("Horizontal");
		float inputY = Input.GetAxis("Vertical");
		
		// 4 - Movement per direction
		movement = new Vector2(
			speed.x * inputX,
			speed.y * inputY);

		// 5 - Shooting
		bool shoot = Input.GetButtonDown ("Fire1");
		shoot |= Input.GetButtonDown ("Fire2");

		if (shoot) {
						ActorWeapon weapon = GetComponent<ActorWeapon> ();
						if (weapon != null) {
								weapon.Attack (false);
						}
				}
		
	}
	
	void FixedUpdate()
	{
		// 5 - Move the game object
		rigidbody2D.velocity = movement;
	}

    public void OnCollisionEnter2D(Collision2D other)
    {
		if(other.gameObject.tag == "Waypoint")
		{	 
			currentWaypoint = other.gameObject.GetComponent<Waypoint>().WaypointNumber;
		}
	}
}
