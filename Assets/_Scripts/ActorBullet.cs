﻿using UnityEngine;

public class ActorBullet : MonoBehaviour {

	public int damage = 1;

	public bool isEnemyShot = false;

	public Vector2 speed = new Vector2(0,10);

	public Vector2 direction = new Vector2 (0, 1);

	private Vector2 movement;

	void Update() {
		movement = new Vector2 (
			speed.x * direction.x,
			speed.y * direction.y);
	}

	// Use this for initialization
	void Start () {
		Destroy (gameObject, 20);
	}

	void FixedUpdate() {
		rigidbody2D.velocity = movement;
	}
}
