﻿using UnityEngine;
using System.Collections;

public class ActorWeapon : MonoBehaviour {

	public Transform shotPrefab;

	public float shootingRate = 0.25f;

	private float shootCooldown;

	// Use this for initialization
	void Start () {
		shootCooldown = 0f;
	}
	
	// Update is called once per frame
	void Update () {
		if (shootCooldown > 0) {
			shootCooldown -= Time.deltaTime;
				}
	}

	public void Attack(bool isEnemy)

	{
		if (CanAttack) {
			shootCooldown = shootingRate;

			var shotTransfrom = Instantiate(shotPrefab) as Transform;

			shotTransfrom.position = transform.position;

			ActorBullet shot = shotTransfrom.gameObject.GetComponent<ActorBullet>();

			if (shot != null)
			{
				shot.isEnemyShot = isEnemy;
			}

			ActorBullet move = shotTransfrom.gameObject.GetComponent<ActorBullet>();
			if (move != null)
			{
				move.direction = this.transform.up;
			}

				}
	}

	public bool CanAttack {
				get {
						return shootCooldown <= 0f;
				}
		}
}
